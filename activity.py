from abc import ABC

class Animal(ABC):
    def eat(self, food):
        pass

    def make_sound(self):
        pass

class Dog(Animal):
    def __init__(self, name, age, breed):
        super().__init__()

        self._name = name
        self._age = age
        self._breed = breed

    def get_name(self):
        print(f"Name is {self._name}!")

    def set_name(self, name):
        self._name = name

    def get_age(self):
        print(f"Age is {self._age}.")

    def set_age(self, age):
        self._age = age

    def get_breed(self):
        print(f"Breed is {self.breed}.")

    def set_breed(self, breed):
        self._breed = breed

    def eat(self, food):
        print(f"Eaten {food}!")

    def make_sound(self):
        print(f"Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self._name}!")

class Cat(Animal):
    def __init__(self, name, age, breed):
        super().__init__()

        self._name = name
        self._age = age
        self._breed = breed

    def get_name(self):
        print(f"Name is {self._name}")

    def set_name(self, name):
        self._name = name

    def get_age(self):
        print(f"Age is {self._age}.")

    def set_age(self, age):
        self._age = age

    def get_breed(self):
        print(f"Breed is {self.breed}.")

    def set_breed(self, breed):
        self._breed = breed

    def eat(self, food):
        print(f"Serve me {food}!")

    def make_sound(self):
        print(f"Miaow! Nyaw! Nyaaaaa!")

    def call(self):
        print(f"{self._name}, come on!")

dog1 = Dog("Isis", 15, "Dalmatian")
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", 4, "Persian")
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()